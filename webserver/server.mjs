import path from 'node:path';
import child_process from 'node:child_process';

import Fastify from 'fastify';
import fastifyBasicAuth from '@fastify/basic-auth';
import fastifyStatic from '@fastify/static';
import formbody from "@fastify/formbody";

const __dirname = import.meta.dirname;

// Create a Fastify instance
const fastify = Fastify({
    logger: true
});

// Add basic authentication middleware
fastify.register(fastifyBasicAuth, {
    validate(username, password, req, res, done) {
        if (username === 'user' && password === 'glhf') {
            done();
        } else {
            done(new Error('Authentication failed'));
        }
    },
    authenticate: true,
});

fastify.register(formbody);

fastify.register(fastifyStatic, {
    root: path.join(__dirname, 'public'),
    prefix: '/public/',
});



fastify.after(() => {
    fastify.route({
        method: "POST",
        url: '/restart',
        onRequest: fastify.basicAuth,
        handler: async function (req, res) {
            restart();
            res.redirect('/success');
        },
    })
});


fastify.get('/', function (req, res) {
    res.sendFile('index.html');
})

fastify.get('/success', async function (req, res) {
    return res.sendFile('success.html')
})


// Start the server
try {
    await fastify.listen({
        port: 80,
        host: '::'
    });
    console.log('Server listening on port 80');
} catch (err) {
    fastify.log.error(err);
    process.exit(1);
}


function restart() {
    child_process.exec("pz_restart", (error, stdout, stderr) => {
        console.log(stdout);
    });
}
