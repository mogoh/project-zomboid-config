# Project Zomboid Config

- https://developer.valvesoftware.com/wiki/SteamCMD#Linux
- https://stackoverflow.com/questions/76688863/apt-add-repository-doesnt-work-on-debian-12
- https://pzwiki.net/wiki/Dedicated_server
- https://pzidgrabber.com/
- https://steamcommunity.com/sharedfiles/filedetails/?id=3217775221
- https://steamcommunity.com/sharedfiles/filedetails/?id=3218690700

## Setup

```bash
sudo apt-get update
sudo apt-get upgrade
sudo add-apt-repository multiverse
sudo dpkg --add-architecture i386
sudo apt-get install software-properties-common -y
sudo apt-add-repository non-free
sudo apt-get update -y
sudo apt-get upgrade
sudo apt-get install steamcmd -y
```

## Install Project Zomboid

```bash
sudo adduser project-zomboid
sudo -u project-zomboid -i
```

```bash
cat >$HOME/update_zomboid.txt <<'EOL'
// update_zomboid.txt
//
@ShutdownOnFailedCommand 1 //set to 0 if updating multiple servers at once
@NoPromptForPassword 1
//for servers which don't need a login
force_install_dir /home/project-zomboid/pzserver/
login anonymous
app_update 380870 validate
quit
EOL

export PATH=$PATH:/usr/games
steamcmd +runscript $HOME/update_zomboid.txt

mkdir --parents $HOME/Zomboid/mods
mkdir --parents $HOME/src
git clone git@gitlab.com:mogoh/project-zomboid-config.git $HOME/src/project-zomboid-config/
ln -s $HOME/src/project-zomboid-config/Server $HOME/Zomboid/Server
```

As root:

```bash
cp /home/project-zomboid/src/project-zomboid-config/zomboid.service /etc/systemd/system/zomboid.service
cp /home/project-zomboid/src/project-zomboid-config/zomboid.socket /etc/systemd/system/zomboid.socket
```

## Other

```
cat >>$HOME/.bashrc <<'EOL'

export PATH=${HOME}/.bin:${PATH}

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi
unset rc
EOL
. ${HOME}/.bashrc
ln -s ${HOME}/src/project-zomboid-config/bin ${HOME}.bin
```

```
sudo apt-get install cron -y
```

```
apt-get install nodejs npm -y
npm install -g corepack
```

```
# allow unprivileged users to use port 80 and upwards
cat <<EOF | tee "/etc/sysctl.d/99-reduce-unprivileged-port-start-to-80.conf" >/dev/null
net.ipv4.ip_unprivileged_port_start=80
EOF
```

## systemd

### start a server

```bash
systemctl start zomboid.socket
```

### stop a server

```bash
systemctl stop zomboid
```

### restart a server

```bash
systemctl restart zomboid
```

### check server status (ctrl-c to exit)

```bash
systemctl status zomboid
```

## tmux

### New session

```bash
tmux new-session -A -s pz
```

### Detach

[CTRL]+[b] [d]

### Re-Attach

```bash
tmux attach-session -t pz
```

### Start

```bash
cd ${HOME}/pzserver/ && bash start-server.sh -Xms2048m -Xmx6144m
```

## Wipe

```bash
rm -rf ${HOME}/Zomboid/Logs ${HOME}/Zomboid/Lua ${HOME}/Zomboid/backups ${HOME}/Zomboid/db ${HOME}/Zomboid/messaging ${HOME}/Zomboid/options.ini ${HOME}/Zomboid/server-console.txt ${HOME}/Zomboid/Saves
```

## Daily restart (cron + tmux)

Remember time is UTC.
Restart at 6 o'clock.

```
0 4 * * * /home/project-zomboid/src/project-zomboid-config/bin/pz_restart
```

## Webserver

```bash
tmux new-session -A -s pz_webserver
cd /home/project-zomboid/src/project-zomboid-config/webserver/
npm install
npm run server
```
