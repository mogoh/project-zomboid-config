#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail


LOCKFILE="${HOME}/.pz_restart.lock"
MAX_LOCK_AGE=600 # Maximum age of the lock file in seconds (e.g., 10 minutes)

# Check if the lock file exists
if [[ -e "${LOCKFILE}" ]]; then
    # Check the age of the lock file
    LOCKFILE_AGE=$(($(date +%s) - $(stat -c %Y "$LOCKFILE")))
    
    if [[ "${LOCKFILE_AGE}" -lt "${MAX_LOCK_AGE}" ]]; then
        echo "Another instance of the script is already running. Exiting."
        exit 1
    else
        echo "Stale lock file found. Removing and continuing."
        rm "${LOCKFILE}"
    fi
else
    echo "Restarting the server ..."
fi

# Create the lock file
touch "${LOCKFILE}"


# Restart logic
tmux send-keys -t pz "servermsg \"Server is restarting in 1 minute.\"" Enter
sleep 1m
tmux send-keys -t pz "quit" Enter
sleep 1m
killall -9 ProjectZomboid64 || true
sleep 10s
tmux send-keys -t pz "cd ${HOME}/pzserver/" Enter
tmux send-keys -t pz "${HOME}/pzserver/start-server.sh" Enter

# Remove the lock file when script execution is finished
rm "${LOCKFILE}"
